#ifndef MQCUSTOMPLOT_H
#define MQCUSTOMPLOT_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QDateTime>
#include "qcustomplot.h"

class MQCustomPlot : public QCustomPlot
{
    Q_OBJECT
private:
    QString dbName;
    QString tableName;
public:
    MQCustomPlot(QWidget* parent=0);
public slots:
    void mouseMoveEvent(QMouseEvent *event) override;
public:
    void setDB(QString dbName, QString tableName, QString yLabel);
    void insertValue(double key, double value);
};

#endif // MQCUSTOMPLOT_H
